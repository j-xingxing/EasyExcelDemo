package com.litchishell.easyexcel.controller;

import com.litchishell.easyexcel.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author LitchiShell
 * @description:
 * @create 2021-11-28
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService service;

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file) {
        service.upload(file);
        return "success";
    }
}
