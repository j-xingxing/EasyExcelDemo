package com.litchishell.easyexcel.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LitchiShell
 * @description:
 * @create 2021-11-28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    /**
     * 自增id
     */
    private Integer id;

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 性别
     */
    private String gender;

    /**
     * 年龄
     */
    private Integer age;
}
